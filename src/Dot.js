var Dot = cc.Sprite.extend({
    
    ctor: function() {
        this._super();
    },
    
    update: function() {
        this.move();
    },
    
    randomSpawn: function(){
        var edge = Math.floor( Math.random() * 4) + 1;
        var rotated = Math.floor( Math.random() *360);
        if( edge == 1)
            this.randomPosition( ScreenHeight , 20 , 'y' , rotated);
        if( edge == 2)
            this.randomPosition( ScreenHeight , 1000 , 'y' , rotated);
        if( edge == 3)
            this.randomPosition( ScreenWidth , 20 , 'x' , rotated);
        if( edge == 4)
            this.randomPosition( ScreenWidth , 740 , 'x' , rotated);
    },
    
    randomPosition: function( edgeGenerate , sideLock , side , rotated){
        if( side == 'x'){
            var x = 10 + Math.floor( Math.random() * ( edgeGenerate - 10));
            this.setPosition( new cc.p( x , sideLock ));
        }
        if( side == 'y'){
            var y = 10 + Math.floor( Math.random() * ( edgeGenerate - 10));
            this.setPosition( new cc.p( sideLock , y ));
        }
        this.setRotation( rotated );
    },
    
    move: function(){
        var direction = this.getRotation( );
        var pos = this.getPosition();
        var posX = pos.x + Math.sin( direction * Math.PI/180 ) * Dot.Speed;
        var posY = pos.y + Math.cos( direction * Math.PI/180 ) * Dot.Speed;
        if( this.checkOutOfBound( pos ) )
            this.randomSpawn();
        else
            this.setPosition( new cc.p( posX , posY ));
    },
    
    checkOutOfBound: function( pos ){
        if( pos.x > ScreenWidth || pos.x < 0 || pos.y > ScreenHeight || pos.y < 0 )
            return true;
        return false;
    },
    
    checkPlayerHit: function( player ) {
        var playerPos = player.getPosition();
        var dotPos = this.getPosition();
        if( Math.abs( playerPos.x - dotPos.x ) <= Dot.Radius+15 && Math.abs ( playerPos.y - dotPos.y ) <= Dot.Radius+20 )
            return true;
        return false;
    }
     
});

Dot.Radius = 7.5;
Dot.Speed = 3;