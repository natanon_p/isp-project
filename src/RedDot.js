var RedDot = Dot.extend({
    
    ctor: function() {
        this._super();
        this.initWithFile( 'res/images/RedDot.png' );
    },
    
    hitEffect: function( player , score ){
        player.setPosition(1800,1000);
        player.removeFromParent();
    }
});
