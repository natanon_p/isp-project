var GreenDot = Dot.extend({
    
    ctor: function() {
        this._super();
        this.initWithFile( 'res/images/GreenDot.png' );
    },
    
    hitEffect: function( player ){
        player.hitted = true;
    }
});
