var GameLayer = cc.LayerColor.extend({
    
    init : function() {
        this._super( cc.p( 0, 0, 0, 0 ) );
        this.setPosition( cc.p( 0, 0 ) );
        this.DotArray = [];
        this.Score = 0;
        this.Counter = 0;
        this.started = false;
        this.setBackGround();
        this.addPlayer();
        this.addScoreLabel();
        this.addKeyboardHandlers(); 
        this.scheduleUpdate();
    },
    
    setBackGround : function() {
        this.background = cc.Sprite.create( 'res/images/bg.png' );
        this.background.setPosition( cc.p( ScreenWidth/2 , ScreenHeight/2 ));
        this.addChild( this.background , 0 );
    },
    
    update : function() {
        if( this.started ){
            this.pointer.scheduleUpdate();
            var currentScore = this.Score;
            this.checkHit();
            if( this.pointer.slowhitted){
                this.Counter++;
                if( this.Counter > 300)
                    this.pointer.slowhitted = false;
                }
            if( this.pointer.valuehitted && this.Score >= 4 * Math.ceil(this.Score/4))
                this.addRedDots();
            else if( this.Score % 4 == 0 && this.Score != currentScore)
                this.addRedDots();
            if( this.Score > 6)
                this.randomaddDot();
            this.scoreLabel.setString( this.Score );
        }
    },
    
    addPlayer : function() {
        this.pointer = new Player();
        this.pointer.setPosition( cc.p( ScreenWidth/2 , ScreenHeight/2 ) );
        this.addChild( this.pointer );
    },
    
    addRedDots : function() {
        this.addDot( 'red' );
    },
    
    addGreenDots : function() {
        for(var i = 0; i < 9; i++){
            this.addDot( 'green' );
        }
    },
    randomaddDot : function() {
        var ran = Math.floor( Math.random() * 1000) + 1;
        if(ran <= 5)
            this.addDot( 'yellow' );
        if(ran > 5 && ran <= 9)
            this.addDot( 'blue' );
    },
    addScoreLabel : function(){
        this.scoreLabel = cc.LabelTTF.create( this.Score , 'Irial' , 40);
        this.scoreLabel.setPosition( ScreenWidth - 50 , ScreenHeight - 50);
        this.addChild( this.scoreLabel );
    },
    checkHit : function() {
        for(var i = 0; i < this.DotArray.length; i++){
            if( this.DotArray[i].checkPlayerHit( this.pointer ) ){
                this.DotArray[i].hitEffect( this.pointer );
                if( this.pointer.hitted)
                    this.Score++;
                if( this.pointer.valuehitted)
                    this.Score += 3;
                if( !this.pointer.slowhitted)
                    this.Counter = 0;
                this.DotArray[i].randomSpawn(); 
            }
            else
                this.pointer.hitted = false;
                this.pointer.valuehitted = false;
        }
    },
    
    addDot : function( type ){
        if( type == 'red')
            var dot = new RedDot();
        if( type == 'green')
            var dot = new GreenDot();
        if( type == 'yellow')
            var dot = new YellowDot();
        if( type == 'blue')
            var dot = new BlueDot();
        dot.randomSpawn();
        dot.scheduleUpdate();
        this.DotArray.push( dot );
        this.addChild( dot );
    },
    
    addAllDots : function(){
        this.addRedDots();
        this.addRedDots();
        this.addGreenDots();
    },
    
    addKeyboardHandlers: function() {
        var self = this;
        cc.eventManager.addListener({
            event: cc.EventListener.KEYBOARD,
            onKeyPressed : function( keyCode, event ) {
                self.onKeyDown( keyCode, event );
            },
            onKeyReleased: function( keyCode, event ) {
                self.onKeyUp( keyCode, event );
            }
        }, this );
    },
 
    onKeyDown: function( keyCode, event ) {
       if( keyCode == GameLayer.ARROWDIR.LEFT ){
            this.pointer.start();
            if(!this.started)
                this.addAllDots();
            this.started = true; 
            this.pointer.isRotateLeft = true;
       }
       if( keyCode == GameLayer.ARROWDIR.RIGHT ){
            this.pointer.start();
            if(!this.started)
                this.addAllDots();
            this.started = true;
            this.pointer.isRotateRight = true;
       }
    },
 
    onKeyUp: function( keyCode, event ) {
       if( keyCode == GameLayer.ARROWDIR.LEFT ){
            this.pointer.isRotateLeft = false;
       }
       if( keyCode == GameLayer.ARROWDIR.RIGHT ){
            this.pointer.isRotateRight = false;
       }
    }
    
});

var StartScene = cc.Scene.extend({
    
    onEnter: function() {
        this._super();
        var layer = new GameLayer();
        console.log( 'GameCreate' );
        layer.init();
        this.addChild( layer );
    }
    
});

GameLayer.ARROWDIR = {
    LEFT : cc.KEY.left,
    RIGHT : cc.KEY.right,
    UP : cc.KEY.up,
    DOWN : cc.KEY.down
};