var YellowDot = Dot.extend({
    
    ctor: function() {
        this._super();
        this.initWithFile( 'res/images/YellowDot.png' );
    },
    move: function(){
        var direction = this.getRotation( );
        var pos = this.getPosition();
        var posX = pos.x + Math.sin( direction * Math.PI/180 ) * (Dot.Speed * 2);
        var posY = pos.y + Math.cos( direction * Math.PI/180 ) * (Dot.Speed * 2);
        if( this.checkOutOfBound( pos ) )
            this.removeFromParent();
        else
            this.setPosition( new cc.p( posX , posY ));
    },
    hitEffect: function( player ){
        player.valuehitted = true;
    }
});