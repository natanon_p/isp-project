var Player = cc.Sprite.extend({
    
    ctor: function() {
        this._super();
        this.initWithFile( 'res/images/pointer.png' );
        this.started = false;
        this.isRotateLeft = false;
        this.isRotateRight = false;
        this.rotation = 0;
        this.hitted = false;
        this.valuehitted = false;
        this.slowhitted = false;
    },
    
    start : function(){
        this.started = true;
    },
    
    update: function( dt ){
        if( this.started ){
            this.UpDirection( this.pos );
            this.pos = this.getPosition();
            if( this.isRotateRight ){
                this.rotateRight( );
            }
            else if( this.isRotateLeft ){
                this.rotateLeft( );
            }
        }
    },
    
    UpDirection: function( pos ){
        var direction = this.getRotation( );
        var pos = this.getPosition();
        var posX = pos.x + Math.sin( direction * Math.PI/180 ) * Player.Speed;
        var posY = pos.y + Math.cos( direction * Math.PI/180 ) * Player.Speed;
        if( this.checkOutOfBound( pos ) )
            this.setPointerPosition( pos );
        else
            this.setPosition( new cc.p( posX , posY ));
    },
    
    setPointerPosition: function( pos ){
        if( pos.x > ScreenWidth  )
            this.setPosition( new cc.p( 0 , pos.y ));
        if( pos.x < 0 )
            this.setPosition( new cc.p( ScreenWidth , pos.y ));
        if( pos.y > ScreenHeight )
            this.setPosition( new cc.p( pos.x , 0 ));
        if( pos.y < 0 )
            this.setPosition( new cc.p( pos.x , ScreenHeight ));
    },
    
    checkOutOfBound: function( pos ){
        if( pos.x > ScreenWidth || pos.x < 0 || pos.y > ScreenHeight || pos.y < 0 )
            return true;
        return false;
    },
    
    rotateRight : function( ) {
        if( this.slowhitted)
            this.rotation += Player.Turn - 1;
        else
            this.rotation += Player.Turn;
        this.setRotation( this.rotation % 360 );
    },
    
    rotateLeft : function( ) {        
        if( this.slowhitted)
            this.rotation -= Player.Turn - 1;
        else
            this.rotation -= Player.Turn;
        this.setRotation( this.rotation % 360 );
    }
    
});

Player.DIR = {
    RIGHT: 1,
    LEFT: 2,
    UP: 3
};
Player.Turn = 3;
Player.Speed = 5;